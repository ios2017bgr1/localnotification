//
//  ViewController.swift
//  notifications+userdefault
//
//  Created by Sebas on 12/1/18.
//  Copyright © 2018 Sebas. All rights reserved.
//

import UIKit
import UserNotifications

class ViewController: UIViewController {
    
    @IBOutlet weak var inputTextField: UITextField!
    @IBOutlet weak var outputLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        outputLabel.text = UserDefaults.standard.object(forKey: "mensaje") as? String
        
        UNUserNotificationCenter.current().requestAuthorization(options: [.sound, .alert, .badge]) { (granted, error) in
            // handle error
        }
        
    }
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        
        outputLabel.text = inputTextField.text
        
        UserDefaults.standard.set(outputLabel.text, forKey: "mensaje")
        
        setNotification()
    }
    
    func setNotification(){
        
//        1. importar UserNotifications
//        2. requestAuthorization
//        3. crear contenido
        
        let content = UNMutableNotificationContent()
        content.title = "Super titutlo"
        content.subtitle = "Subtitulo"
        content.body = "el Mensaje es: \(outputLabel.text!)"
        content.badge = UIApplication.shared.applicationIconBadgeNumber + 1 as NSNumber
        
//        4. crear trigger
       
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 15, repeats: false)
        
//        5. crear request
        
        let notificationRequest = UNNotificationRequest(identifier: "notification1", content: content, trigger: trigger)
        
//        6. añadir el request al UNUserNotificationCenter
        
        UNUserNotificationCenter.current().add(notificationRequest) { (error) in
            // handle error
        }
        
    }
    
    
    
}

